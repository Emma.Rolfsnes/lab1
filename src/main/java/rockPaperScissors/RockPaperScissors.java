package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

    
         while(true) {
            String computerChoice = rpsChoices.get(new Random().nextInt(rpsChoices.size()));
            System.out.println("Let's play round " + roundCounter);
            String humanChoice = "";
            while (true) {
                humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
                if(rpsChoices.contains(humanChoice)) {
                    break;
                }
                else {
                    System.out.format("I do not understand %s %n", humanChoice); /** Hvis du ikke skriver inn r/p/s */
                    continue;
                }
            }

            if(humanChoice.equals(computerChoice)) {
                System.out.format("Human chose %s, computer chose %s. It's a tie!", humanChoice, computerChoice);
            }

            if((humanChoice.equals("rock")) && (computerChoice.equals("scissors")) || (humanChoice.equals("scissors")) && (computerChoice.equals("paper")) || (humanChoice.equals("paper")) && (computerChoice.equals("rock")) ) {
                System.out.format("Human chose %s, computer chose %s. Human wins!", humanChoice, computerChoice);
                humanScore ++;
            }

            if((computerChoice.equals("rock")) && (humanChoice.equals("scissors")) || (computerChoice.equals("scissors")) && (humanChoice.equals("paper")) || (computerChoice.equals("paper")) && (humanChoice.equals("rock")) ) {
                System.out.format("Human chose %s, computer chose %s. Computer wins!", humanChoice, computerChoice);
                computerScore ++;
            }
            System.out.format("%nScore: human %s, computer %s", humanScore, computerScore);

            String newRound = readInput("\nDo you wish to continue playing? (y/n)?");
            if (newRound.equals("n")) {
                System.out.format("Bye bye :)");
                break;
            }

            else {
                roundCounter ++;
            }
        
        }
        // TODO: Implement Rock Paper Scissors
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
